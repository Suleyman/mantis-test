
package com.mantis.component.datepicker;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.context.ResponseWriter;
import javax.faces.render.FacesRenderer;
import javax.faces.render.Renderer;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@FacesRenderer(rendererType = Datepicker.DEFAULT_RENDERER, componentFamily = Datepicker.COMPONENT_FAMILY)
public class DatepickerRenderer extends Renderer {

    @Override
    public void decode(FacesContext context, UIComponent component) {
        super.decode(context, component);
    }

    @Override
    public void encodeBegin(FacesContext context, UIComponent component) throws IOException {
        ResponseWriter writer = context.getResponseWriter();
        writer.startElement("input", component);
        writer.writeAttribute("class", "form-control", null);
        writer.writeAttribute("type", "date", null);

        String now = LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        writer.writeAttribute("value", now, null);

    }

    @Override
    public void encodeEnd(FacesContext context, UIComponent component) throws IOException {
       context.getResponseWriter().endElement("input");
    }

}
